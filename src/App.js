import { useState, useEffect } from "react";
import "./App.css";
import { distanceCalculate } from "./distance_calculate";
import { api, API_KEY } from "./api_key";

const testData = {
  latitude: 48.184029,
  longitude: 16.348709,
  // my adress Bradnmayergasse 18 1050
};

const App = () => {
  const [adress, setAdress] = useState("");
  const [loading, setLoading] = useState(false);
  const [pointData, setPointData] = useState("");
  const [people, setPeople] = useState();

  const handleChange = (e) => {
    setPointData(e.target.value);
  };

  const findPeople = async () => {
    try {
      const getPositionData = await fetch(
        `${api}access_key=${API_KEY}&query=${pointData}&limit=1&output=json`
      );

      const geoData = await getPositionData.json();

      console.log("geo data", geoData.data);

      const users = await fetch(
        "https://run.mocky.io/v3/28f4de1f-0c75-4a5f-bf5c-2d2b485285e2"
      );

      const data = await users.json();
      console.log(data);
      const mySearch = await data.users.map(
        (user) =>
          (user.distance_from_me = distanceCalculate(
            geoData.data[0].latitude,
            geoData.data[0].longitude,
            user.latitude,
            user.longitude
          ))
      );

      console.log("my search", data.users);

      const final = data.users.filter(
        (user) => user.distance_from_me <= user.service_offer
      );
      setPeople(final);
    } catch (error) {
      console.log("Error", error);
    }
  };

  return (
    <div className="App">
      TEST...
      <p>Latitude: {pointData.latitude}</p>
      <p>Longitude: {pointData.longitude}</p>
      <input
        type="text"
        placeholder="enter your adress"
        value={pointData}
        onChange={handleChange}
      />
      <button onClick={findPeople}>Search People</button>
      <h4>From closest to farest</h4>
      {people &&
        people.map((user, i) => (
          <div key={i}>
            <p>Name: {user.name}</p>
            <p>Service: {user.service}</p>
            <p>Address: {user.adress}</p>
            <p>Post code: {user.postal_code}</p>
            <p>City: {user.region}</p>
            <p>Service in km: {user.service_offer}</p>
            <h2>DISTANCE FROM ME: {user.distance_from_me}km</h2>
            <p>-------------------------------------</p>
          </div>
        ))}
    </div>
  );
};

export default App;

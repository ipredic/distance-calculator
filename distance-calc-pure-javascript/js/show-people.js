import { distanceCalculate } from "./distance.calculator.js";
import { api, API_KEY } from "./api_key.js";

const findPeople = async () => {
  try {
    const inputVal = document.getElementById("inputAdress").value;
    if (inputVal !== "") {
      const getSearcherPositionData = await fetch(
        `${api}access_key=${API_KEY}&query=${inputVal}&limit=1&output=json`
      );

      const geoSearcherData = await getSearcherPositionData.json();
      //console.log("geoSearcherData", geoSearcherData.data);

      // call your backhand endpoint
      const users = await fetch(
        "https://run.mocky.io/v3/28f4de1f-0c75-4a5f-bf5c-2d2b485285e2"
      );

      const data = await users.json();
      //console.log("users", data);
      const calculateDistances = await data.users.map(
        (user) =>
          (user.distance_from_me = distanceCalculate(
            geoSearcherData.data[0].latitude,
            geoSearcherData.data[0].longitude,
            user.latitude,
            user.longitude
          ))
      );

      const finalResult = data.users.filter(
        (user) => user.distance_from_me <= user.service_offer
      );

      renderList(finalResult);
      document.getElementById("inputAdress").value = "";
    }
  } catch (error) {
    console.log("Error", error);
  }
};

const renderList = (result) => {
  const usersAvailable = [];
  const list = document.getElementById("result__list");
  const newP = document.createElement("p");
  newP.setAttribute("id", "userInfoText");
  usersAvailable.push(...result);
  //console.log(usersAvailable);
  list.innerHTML = "";
  usersAvailable.map((user) => {
    // this data depends on your object structure like name, region and etc...
    const renderText = document.createTextNode(`
       Name: ${user.name}
       Service: ${user.service}
       Address: ${user.adress}
       Post code: ${user.postal_code}
       City: ${user.region}
       Service in km: ${user.service_offer}
       Distance from my address: ${user.distance_from_me}km
       -------------------------------------
   `);
    newP.appendChild(renderText);
    list.appendChild(newP);
  });
};

document.getElementById("myButton").addEventListener("click", findPeople);
